package my.company.RestTemplateImpl.model;

import java.util.List;

public class CompanyInformation {

    List<Employee> employees;
    List<Department> departments;

    public CompanyInformation() {
    }

    public CompanyInformation(List<Employee> employees, List<Department> departments) {
        this.employees = employees;
        this.departments = departments;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}
