package my.company.RestTemplateImpl.controller;

import my.company.RestTemplateImpl.model.CompanyInformation;
import my.company.RestTemplateImpl.model.Employee;
import my.company.RestTemplateImpl.client.EmployeeService;
import my.company.RestTemplateImpl.service.CompanyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanyController {
    @Autowired
    CompanyServiceImpl companyService;

    @RequestMapping("/company-information")
    public CompanyInformation getEmployees(){
        return companyService.retrieveCompanyInformation();
    }

}
