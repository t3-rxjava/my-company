package my.company.RestTemplateImpl.service;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import my.company.RestTemplateImpl.client.DepartmentService;
import my.company.RestTemplateImpl.client.EmployeeService;
import my.company.RestTemplateImpl.model.CompanyInformation;
import my.company.RestTemplateImpl.model.Department;
import my.company.RestTemplateImpl.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    DepartmentService departmentService;

    public CompanyInformation retrieveCompanyInformation() {
        Single<List<Employee>> employees = employeeService.getEmployees().subscribeOn(Schedulers.io());

        Single<List<Department>> departments = departmentService.getDepartments().subscribeOn(Schedulers.io());

        System.out.println("Company Information on Thread "+ Thread.currentThread());

        return new CompanyInformation(employees.blockingGet(), departments.blockingGet());
    }
}
