package my.company.RestTemplateImpl.client.impl;

import io.reactivex.Single;
import my.company.RestTemplateImpl.client.EmployeeService;
import my.company.RestTemplateImpl.model.Employee;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public Single<List<Employee>> getEmployees() {

        Single<List<Employee>> employeeSingle = Single.create(sub ->
                sub.onSuccess(getResponse()));

        return employeeSingle;
    }

    private List<Employee> getResponse() {
        System.out.println("==========Retrieving GET Employees========= on thread "+Thread.currentThread());

        return restTemplate.exchange("http://localhost:8081/employees", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Employee>>() {
                }).getBody();
    }
}
