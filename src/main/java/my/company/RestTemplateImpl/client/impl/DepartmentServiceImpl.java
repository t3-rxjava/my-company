package my.company.RestTemplateImpl.client.impl;

import io.reactivex.Single;
import my.company.RestTemplateImpl.client.DepartmentService;
import my.company.RestTemplateImpl.model.Department;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class DepartmentServiceImpl  implements DepartmentService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public Single<List<Department>> getDepartments() {

        return Single.create(sub -> sub.onSuccess(getResponse()));
    }

    private List<Department> getResponse() {
        System.out.println("==========Retrieving GET Departments========= on thread "+Thread.currentThread());

        return restTemplate.exchange("http://localhost:8082/downstream/departments", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Department>>() {
                }).getBody();
    }
}
