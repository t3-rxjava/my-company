package my.company.RestTemplateImpl.client;

import io.reactivex.Single;
import my.company.RestTemplateImpl.model.Department;

import java.util.List;

public interface DepartmentService {

    public Single<List<Department>> getDepartments();
}
