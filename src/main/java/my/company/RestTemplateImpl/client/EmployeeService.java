package my.company.RestTemplateImpl.client;

import io.reactivex.Single;
import my.company.RestTemplateImpl.model.Employee;

import java.util.List;

public interface EmployeeService {
    Single<List<Employee>> getEmployees();
}
